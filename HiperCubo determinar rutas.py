
from turtle import *
from Tkinter import *



class Nodo:
    def __init__(self,x,y,horizontal,arco,vertical,diagonalx,diagonaly,nombre):
        self.x = x
        self.y = y
        self.h = horizontal
        self.a = arco
        self.v = vertical
        self.dx = diagonalx
        self.dy = diagonaly
        self.nombre = nombre


def xor(x, y):
    ans = ""
    for i in xrange(len(x)):
        if x[i] == "0" and y[i] == "1" or x[i] == "1" and y[i] == "0":
            ans += "1"
        else:
            ans += "0"
    return ans

def ruta():
    
    setup(1200, 800, 0, 0)
    screensize(1200, 800)
    clearscreen()
    pensize(3)
    color("black")
    x = entradae.get()
    y = entradad.get()

    write("De "+x+" a "+y)
    salidat = Label(ventana, text = "tag = ")
    salida = Label(ventana, text = xor(x,y))
    salida.grid(column = 4, row = 10, padx = (10,10), pady = (10,10))
    salidat.grid(column = 2, row = 10, padx = (10,10), pady = (10,10))
   

    tag = xor(x,y)
    camino = determinarRuta(tag)

    speed(10)
    lado = 200
    arc= 600
    
    diagonal = 120
    
    nodos = []

    nodos.append(Nodo(-300,100,lado,arc,-lado,-diagonal,-diagonal,"0000")) #0
    nodos.append(Nodo(-100,100,-lado,arc,-lado,-diagonal,-diagonal,"0001")) #1
    nodos.append(Nodo(-420,-20,lado,arc,-lado,diagonal,diagonal,"0010")) #2
    nodos.append(Nodo(-220,-20,-lado,arc,-lado,diagonal,diagonal,"0011")) #3
    nodos.append(Nodo(-300,-100,lado,arc,lado,-diagonal,-diagonal,"0100")) #4
    nodos.append(Nodo(-100,-100,-lado,arc,lado,-diagonal,-diagonal,"0101")) #5
    nodos.append(Nodo(-420,-220,lado,arc,lado,diagonal,diagonal,"0110")) #6
    nodos.append(Nodo(-220,-220,-lado,arc,lado,diagonal,diagonal,"0111")) #7

    incx = 600
    
    nodos.append(Nodo(-300 + incx,100,lado,-arc,-lado,-diagonal,-diagonal,"1000")) #8
    nodos.append(Nodo(-100+ incx,100,-lado,-arc,-lado,-diagonal,-diagonal,"1001")) #9
    nodos.append(Nodo(-420 + incx,-20,lado,-arc,-lado,diagonal,diagonal,"1010")) #10
    nodos.append(Nodo(-220 + incx,-20,-lado,-arc,-lado,diagonal,diagonal,"1011")) #11
    nodos.append(Nodo(-300 + incx,-100,lado,-arc,lado,-diagonal,-diagonal,"1100")) #12
    nodos.append(Nodo(-100 + incx,-100,-lado,-arc,lado,-diagonal,-diagonal,"1101")) #13
    nodos.append(Nodo(-420 + incx,-220,lado,-arc,lado,diagonal,diagonal,"1110")) #14
    nodos.append(Nodo(-220 + incx,-220,-lado,-arc,lado,diagonal,diagonal,"1111")) #15



    for n in nodos:
        regresa(n)
        dot(20, "blue")
        goto(n.x+20,n.y+5)
        write(n.nombre)
        regresa(n)
        goto(n.x + n.h,n.y)
        regresa(n)
        goto(n.x,n.y + n.v)
        regresa(n)
        goto(n.x + n.dx,n.y + n.dy)
        regresa(n)

    nodoActual = nodos[int(x, 2)]
    color("orange")

    pensize(6)
    regresa(nodoActual)
    dot(20, "green")
    for item in camino:
        
            
        speed(1)
        if item == "Arco":
            goto(nodoActual.x,nodoActual.y+100)
            nodoActual = actualizaNodo(nodoActual.x+nodoActual.a,nodoActual.y,nodos)
            goto(nodoActual.x,nodoActual.y+100)
            goto(nodoActual.x,nodoActual.y)
        if item == "Vertical":
            nodoActual = actualizaNodo(nodoActual.x,nodoActual.y + nodoActual.v,nodos)
            goto(nodoActual.x,nodoActual.y)
        if item == "Diagonal":
            nodoActual = actualizaNodo(nodoActual.x + nodoActual.dx,nodoActual.y + nodoActual.dy,nodos)
            goto(nodoActual.x,nodoActual.y)
        if item == "Horizontal":
            nodoActual = actualizaNodo(nodoActual.x+nodoActual.h,nodoActual.y,nodos)
            goto(nodoActual.x,nodoActual.y)
    
        dot(20, "yellow")

    dot(20, "red")
       


def actualizaNodo(x,y,nodos):
    for n in nodos:
        if x == n.x and y == n.y: return n
    return -1

        
    


def determinarRuta(binario):
    ruta = []
    for i in range(len(binario)):
        if binario[i] == '1':
            if(i) == 0:
                ruta.append("Arco")
            if(i) == 1:
                ruta.append("Vertical")
            if(i) == 2:
                ruta.append("Diagonal")
            if(i) == 3:
                ruta.append("Horizontal")
    return ruta

def salir():
    exit()

def regresa(n):
    up()
    goto(n.x,n.y)
    down()

   
        



ventana = Tk()
ventana.title("Hipercubo - Jovanny Rch")
ventana.geometry('300x300')


emisor= Label(ventana, text ="Nodo emisor: ")
emisor.grid(column = 2, row = 3, padx = (10,10), pady = (10,10))
entradae = Entry(ventana)
entradae.grid(column = 4, row = 3, padx = (10,10), pady = (10,10))

destino= Label(ventana, text ="Nodo destino: ")
destino.grid(column = 2, row = 5, padx = (10,10), pady = (10,10))
entradad = Entry(ventana)
entradad.grid(column = 4, row = 5, padx = (10,10), pady = (10,10))

ingreso = Button(ventana, text = "Ingresar", command = ruta)
ingreso.grid(column = 4, row = 7, padx = (10,10), pady = (10,10))

boton_salir= Button(ventana, text= "Salir", bg= "salmon", command= salir)
boton_salir.grid(column= 4, row= 13)




ventana.mainloop()
